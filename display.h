//
//  display.h
//  pgOWL
//
//  Created by Dinesh Kumar on 04/12/14.
//  Copyright (c) 2014 OpenSCG. All rights reserved.
//

#ifndef pgOWL_display_h
#define pgOWL_display_h
#include <ncurses.h>

#define TOTAL_CHARTS 8

enum CHART_MODE {LINE_CHART, TABLE_CHART};

enum CHARTS { LOAD, MEMORY, SWAP, IOWAIT };




typedef struct OwlWindow OwlWindow;
typedef struct Plot Plot;
typedef struct OwlLineChart OwlLineChart;

struct Plot {
    int x, y;
};

struct OwlWindow {
    
    Plot OptionsArea;
    Plot ChartArea;
    Plot SummaryArea;
    Plot TimeArea;
    Plot HeaderArea;
    Plot ChatHeaderArea;
    Plot ScreenArea;
    WINDOW *window,*title,*summary;
    
    // Init method
    void (*OwlWindowInit)(OwlWindow*);
    
    // Get methods
    //
    Plot (*GetOptionsArea)(OwlWindow*);
    Plot (*GetChartArea)(OwlWindow*);
    Plot (*GetSummaryArea)(OwlWindow*);
    Plot (*GetTimeArea)(OwlWindow*);
    Plot (*GetHeaderArea)(OwlWindow*);
    Plot (*GetChartHeaderArea)(OwlWindow*);
    Plot (*GetScreenArea)(OwlWindow*);
    
    // Set methods
    //
    void (*SetOptionsArea)(OwlWindow*, Plot);
    void (*SetChartArea)(OwlWindow*, Plot);
    void (*SetSummaryArea)(OwlWindow*, Plot);
    void (*SetTimeArea)(OwlWindow*, Plot);
    void (*SetHeaderArea)(OwlWindow*, Plot);
    void (*SetChartHeaderArea)(OwlWindow*, Plot);
    void (*SetScreenArea)(OwlWindow*, Plot);
};

typedef struct LineDataPoints {
    unsigned int Value;
    Plot Point;
    struct LineDataPoints* NextPoint;
    struct LineDataPoints* Start;
    struct LineDataPoints* End;
    struct LineDataPoints* CurrentPoint;
}LineDataPoints;

typedef struct LineChartPref {
    
    Plot ParWinXAxis, ParWinYAxis, ChartWidth, ChartHeight;
    Plot *CurrPlotPosition;
    LineDataPoints *Points;
    short int YUnitStepCounter, XUnitStepCounter;
    short int NoOfPlots;
    char ChartName[20];
    
}LineChartPref;

struct OwlLineChart {
    Plot ChartArea;
    Plot ChartWidthHeight;
    WINDOW* Chart;
    LineChartPref* Pref;

    // Init method
    void (*OwlLineChartInit)(OwlLineChart*);
    
    // Get Methods
    //
    Plot (*GetOwlLineChartArea)(OwlLineChart*);
    Plot (*GetOwlLineChartWidthHeight)(OwlLineChart*);
    char* (*GetOwlLineChartName)(OwlLineChart*);
    
    // Set Methods
    //
    void (*SetOwlLineChartArea)(OwlLineChart*, Plot);
    void (*SetOwlLineChartWidthHeight)(OwlLineChart*, Plot);
    void (*SetOwlLineChartName)(OwlLineChart*, char*);
    
    
    // Pref Methods
    //
    void (*SetOwlLineChartPosition)(OwlLineChart*, Plot, Plot);
    void (*SetOwlLineChartSize)(OwlLineChart*, Plot, Plot);
    
    // Draw Chart Window
    void (*DrawChartWindow)(OwlLineChart*);
    // Functional Methods
    //
    
    void (*NewOwlLineChart)(OwlLineChart*, Plot, Plot);
};

void LineChartPlot(OwlLineChart *chart, int Value);
void AdjustLineChart(OwlLineChart* chart);
void DrawLineChart(OwlLineChart* chart);
unsigned int InsertDataPoint(OwlLineChart* chart, unsigned int Value);
void ClearRoundTripPointValue(WINDOW* win, const Plot Point);
void ChangePrevPointSymbol(WINDOW* win, const Plot Point);
void ClearRoundTripPointValue(WINDOW* win, const Plot Point);


#endif
