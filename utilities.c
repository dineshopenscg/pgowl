//
//  utilities.c
//  pgOWL
//
//  Created by Dinesh Kumar on 04/12/14.
//  Copyright (c) 2014 OpenSCG. All rights reserved.
//

#include <stdio.h>
#include <unistd.h>
#include <math.h>
#include <termios.h>
#include "display.h"
#include "libpq-fe.h"
#include "dbactivities.h"
#include "sys/socket.h"

Plot GetScreenXY() {
    
    Plot ScreenXY;
    int X, Y;
    getmaxyx(stdscr, Y, X);
    ScreenXY.x = X;
    ScreenXY.y = Y;
    return ScreenXY;
}

/*
 * IsScrDimeChange
 *
 *  This function checks, whether the screen dimension is changed or not.
 *  If the dimension changed, then it update the screen dimension 
 *  to present screen dimension.
 */

int IsScrDimeChange(Plot* _1SecBefore) {
    
    Plot PresentScr = GetScreenXY();
    
    if ( _1SecBefore->x != PresentScr.x || _1SecBefore->y != PresentScr.y) {
        
        _1SecBefore->x = PresentScr.x;
        _1SecBefore->y = PresentScr.y;
        return 1;
    }
    else
        return 0;
}


/*
 * DrawWindow
 *
 *   This function draws window on the screen, and sets 
 *   the required areas PLOT
 */



// XXX: Need to do
// Implement mutex, while owl draws some text on screen
//

void DrawWindow(OwlWindow* win,  PGconn* dbcon) {
    static int val =0;
    Plot    ScreenArea = win->GetScreenArea(win);
    Plot    Tmp;
    int     Linewidth, Lineheight, i;
    
    // IsScrDimeChange (&ScreenArea);
    // Initiating the main header line
    //
    if (win->window || win->title) {
        win->title = newwin(1,ScreenArea.x-2,2,1);
        win->window = newwin(ScreenArea.y,ScreenArea.x,0,0);
        win->summary = newwin(20, 30, 20, 25);
        //win->chart = newwin(10, 70, 5, 20);
        //
        box(win->title,0,0);
        box(win->window,0,0);
    }

    // Priting header
    //
    mvwprintw(win->window,1,(int)(ScreenArea.x)/2-10,"pgOWL");
    Tmp.x = 1;
    Tmp.y = (int)(ScreenArea.x)/2-10;
    win->SetHeaderArea(win, Tmp);
    
    // Printing time with timezone
    // This value will be pg result
    //
    mvwprintw(win->title,1,abs(37-ScreenArea.x),GetDbScalar(dbcon,QueryHashLookup(queryHash, "DBTIMESTAMP")));
    Tmp.x = 1;
    Tmp.y = abs(25-ScreenArea.x);
    win->SetTimeArea(win, Tmp);
    
    Linewidth=(int)(ScreenArea.x)*15/100;
    Lineheight=(ScreenArea.y-2)-(ScreenArea.y-2)*20/100;
    
    // Setting options area
    //
    //mvwprintw(win->window, 3, Linewidth/2-2, "OPTIONS");
    //Tmp.x = 3;
    //Tmp.y = Linewidth/2-2;
    //win->SetOptionsArea(win, Tmp);
    
    // Setting chart area
    //
    mvwprintw(win->window, 3, (ScreenArea.x-Linewidth)/2+10, "CHART AREA");
    Tmp.x = 3;
    Tmp.y = (ScreenArea.x-Linewidth)/2+10;
    win->SetChartArea(win, Tmp);
    
    // Setting Summary Area
    //Tmp.x =20;
    //Tmp.y =25;
    //win->SetSummaryArea(win, Tmp);
    
    // Printing Lines
    //
    //for (i=1; i<=ScreenArea.x-2; i++)
        //mvwprintw(win->window, 4, i, "_");
    
    //for (i=3; i<ScreenArea.y-1; i++)
        //mvwprintw(win->window, i, Linewidth, "|");
    
    //for (i=Linewidth+1; i<ScreenArea.x-1; i++)
        //mvwprintw(win->window, Lineheight, i, "_");
    
    //mvwprintw(win->window, 4, Linewidth, "+");
    //mvwprintw(win->window, Lineheight, Linewidth, "+");
   
    //if (val ==0 ) {
    wrefresh(win->window);
    wrefresh(win->title);
    //wrefresh(win->summary);
    //}
    val++;
}

/*
 *  KbHit
 *
 *          Function returns true, if any key got pressed.
 *          http://linux-sxs.org/programming/kbhit.html
 *
 */

static struct termios initial_settings, new_settings;
static int peek_character = -1;

void init_keyboard()
{
    tcgetattr(0,&initial_settings);
    new_settings = initial_settings;
    new_settings.c_lflag &= ~ICANON;
    new_settings.c_lflag &= ~ECHO;
    new_settings.c_lflag &= ~ISIG;
    new_settings.c_cc[VMIN] = 1;
    new_settings.c_cc[VTIME] = 0;
    tcsetattr(0, TCSANOW, &new_settings);
}

void close_keyboard()
{
    tcsetattr(0, TCSANOW, &initial_settings);
}

int KbHit()
{
    unsigned char ch;
    int nread;
    
    if (peek_character != -1) return 1;
    new_settings.c_cc[VMIN]=0;
    tcsetattr(0, TCSANOW, &new_settings);
    nread = read(0,&ch,1);
    new_settings.c_cc[VMIN]=1;
    tcsetattr(0, TCSANOW, &new_settings);
    if(nread == 1)
    {
        peek_character = ch;
        return 1;
    }
    return 0;
}

int ReadCh()
{
    char ch;
    
    if(peek_character != -1)
    {
        ch = peek_character;
        peek_character = -1;
        return ch;
    }
    read(0,&ch,1);
    return ch;
}


void PrintMessage(WINDOW* Win, Plot P,char* Msg){
    mvwprintw(Win,P.y,P.x, "D");
    wrefresh(Win);
}
