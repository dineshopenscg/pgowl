//
//  servermetric.c
//  pgOWL
//
//  Created by Dinesh Kumar on 06/02/15.
//  Copyright (c) 2015 OpenSCG. All rights reserved.
//

#include "servermetric.h"

/*
 * Get Method
 */

char* GetHostName(ServerMetrics* S) {
    
    return S->HostName;
}

char* GetUpTime(ServerMetrics* S) {
    
    return S->UpTime;
}

float GetLoad1(ServerMetrics* S) {
    
    return S->Load1;
}

float GetLoad5(ServerMetrics* S) {
    
    return S->Load5;
}

float GetLoad15(ServerMetrics* S) {
    
    return S->Load15;
}

char** GetDiskUsage(ServerMetrics* S) {
    
    return NULL;
}

char** GetMemInfo(ServerMetrics* S) {
    
    return S->MemInfo;
}

char** GetVmInfo(ServerMetrics* S) {
    
    return S->VmInfo;
}

char** GetIoStat(ServerMetrics* S) {
    
    return S->IoStat;
}

float GetFreeRam(ServerMetrics* S) {
    return S->FreeRam;
}

float GetSwapUsed(ServerMetrics* S) {
    return S->SwapUsed;
}

float GetIOWait(ServerMetrics* S) {
    return S->IOWait;
}

/*
 * Set Method
 */

void SetHostName(ServerMetrics* S) {
    
    S->HostName = S->ServerConn->ExecuteCmd(S->ServerConn,"hostname");
    S->HostName[strlen(S->HostName)]='\0';
}

void SetUpTime(ServerMetrics* S) {

    char* UpTime=S->ServerConn->ExecuteCmd(S->ServerConn,"uptime");
    
    if (UpTime) {

        /*
         * UPTIME
         *      19:31:22 up 2 days, 22:44,  5 users,  load average: 0.03, 0.03, 0.04
         *
         * As implementation, taking the uptime as first delimited string of ","
         * and ignoring everything upto the charater "l" which is followed by ":",
         * and taking the last 3 float values
         *
         * XXX:
         *
         *     Need to look for a better way to get the exact "load average" values
         */
         
        sscanf(UpTime, "%[^,],%*[^l]%*[^:]:%f,%f,%f", S->UpTime,&(S->Load1),&(S->Load5),&(S->Load15));
        S->UpTime[strlen(S->UpTime)]='\0';
    }
    
    free(UpTime);
}

void SetFreeRam(ServerMetrics* S) {
    char* FreeRam = S->ServerConn->ExecuteCmd(S->ServerConn, "vmstat -SM|grep -A 1 'free'|tail -1|awk -F ' ' '{print $4}'");
    
    if (FreeRam) {
        sscanf(FreeRam, "%f", &(S->FreeRam));
    }
    
    free(FreeRam);
}

void SetSwapUsed(ServerMetrics* S) {
    char* SwapUsed = S->ServerConn->ExecuteCmd(S->ServerConn, "vmstat -SM|grep -A 1 'free'|tail -1|awk -F ' ' '{print $3}'");
    
    if (SwapUsed) {
        sscanf(SwapUsed, "%f", &(S->SwapUsed));
    }
    
    free(SwapUsed);
}


void SetIOWait(ServerMetrics* S) {
    char* IOWait = S->ServerConn->ExecuteCmd(S->ServerConn, "iostat|grep -A 1 iowait|tail -1|awk -F ' ' '{print $4}'");
    
    if (IOWait) {
        sscanf(IOWait, "%f", &(S->IOWait));
    }
    
    free(IOWait);
}


void SetServerConn(ServerMetrics* S, ServerConnection* SCon) {
    
    S->ServerConn = SCon;
}

void ServerMetricInit(ServerMetrics* S) {
    
    S->HostName = (char *) calloc(sizeof(char), MAXHOSTNAMELEN);
    S->UpTime = (char *) calloc(sizeof(char), MAXUPTIMELEN);
    
    /*
     * Get Method
     */
    S->GetHostName = GetHostName;
    S->GetUpTime = GetUpTime;
    S->GetLoad1 = GetLoad1;
    S->GetLoad5 = GetLoad5;
    S->GetLoad15 = GetLoad15;
    S->GetDiskUsage = GetDiskUsage;
    S->GetMemInfo = GetMemInfo;
    S->GetVmInfo = GetVmInfo;
    S->GetIoStat = GetIoStat;
    S->GetFreeRam = GetFreeRam;
    S->GetIOWait = GetIOWait;
    S->GetSwapUsed = GetSwapUsed;
    /*
     * Set Methods
     */
    
    S->SetServerConn = SetServerConn;
    S->SetHostName = SetHostName;
    S->SetUpTime = SetUpTime;
    S->SetFreeRam = SetFreeRam;
    S->SetIOWait = SetIOWait;
    S->SetSwapUsed = SetSwapUsed;
}