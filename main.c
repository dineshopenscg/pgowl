#include "display.h"
#include "serverconnection.h"
#include "servermetric.h"
#include "libpq-fe.h"
#include <stdio.h>
#include "math.h"
#include "stdlib.h"
#include "unistd.h"

extern void OwlWindowInit(OwlWindow*);
extern void ServerConnInit(ServerConnection*);
extern void ServerMetricInit(ServerMetrics*);
extern void OwlLineChartInit(OwlLineChart*);
extern int  IsScrDimeChange(Plot* _1SecBefore);
extern void DrawWindow(OwlWindow* win,const PGconn* con);
extern void LineChartPlot(OwlLineChart* chart, int Value);
extern void AdjustLineChart(OwlLineChart* chart);
extern void DrawLineChart(OwlLineChart* chart);

extern void ClearRoundTripPointValue(WINDOW* win, const Plot Point);
extern void ChangePrevPointSymbol(WINDOW* win, const Plot Point);
extern void ClearRoundTripPointValue(WINDOW* win, const Plot Point);

extern Plot GetScreenXY();
extern int KbHit();
extern char ReadCh();

extern void PrePareLineChart(OwlLineChart* Chart, Plot LinesCols, Plot Area, Plot Height, Plot Width, char* ChartName);

int main(int argc, char *argv[])
{
    OwlWindow OwlWin;
    OwlLineChart LoadChart, MemoryChart, SwapChart, IOWaitChart;
    ServerConnection* S=(ServerConnection *)(calloc(sizeof(ServerConnection), 1));
    ServerMetrics* SM=(ServerMetrics *)(calloc(sizeof(ServerMetrics), 1));
    
    
    S->ServerConnInit=ServerConnInit;
    S->ServerConnInit(S);
    
    SM->ServerMetricInit = ServerMetricInit;
    SM->ServerMetricInit(SM);
    
    if (!argv[1]) {
        puts("HostIP is needed as 1st argument");
        return 1;
    }
    else if(!argv[2]) {
        puts("Username is needed as 2nd argument");
        return 1;
    }
    else if(!argv[3]) {
        puts("Password is needed as 3rd argument");
        return 1;
    }
    S->SetHostIP(S, argv[1]);
    S->SetUserName(S, argv[2]);
    S->SetPassword(S, argv[3]);
    
    S->InitSsh(S);
    S->StartSession(S);
    
    SM->SetServerConn(SM, S);
    SM->SetHostName(SM);
    
    PGconn* con;
    Plot LoadYAxis, LoadXAxis, LineCArea, LineCWidthHeight, MemCArea,MemCWidthHeight;
    Plot SwapCArea, SwapCWidthHeight, IOWaitCArea, IOWaitCWidthHeight;
    
    WINDOW* ChartWindow;
    
    LoadYAxis.y = 1, LoadYAxis.x = 1;
    LoadXAxis.y = 10, LoadXAxis.x = 60;
    
    LineCArea.y = 5, LineCArea.x = 3;
    LineCWidthHeight.y = 15, LineCWidthHeight.x = 70;
    
    MemCWidthHeight = LineCWidthHeight;
    MemCArea.y = 5, MemCArea.x = 80;
    
    SwapCWidthHeight = LineCWidthHeight;
    SwapCArea.y = 25, SwapCArea.x = 3;
    
    IOWaitCWidthHeight = LineCWidthHeight;
    IOWaitCArea.y = 25, IOWaitCArea.x = 80;
    
    //con = PQconnectdb("host=127.0.0.1 dbname=postgres user=postgres password=postgres");
    /* Initializing window */
    OwlWin.OwlWindowInit=OwlWindowInit;
    OwlWin.OwlWindowInit(&OwlWin);
    
    LoadChart.OwlLineChartInit=OwlLineChartInit;
    LoadChart.OwlLineChartInit(&LoadChart);
    
//    MemoryChart.OwlLineChartInit=OwlLineChartInit;
//    MemoryChart.OwlLineChartInit(&MemoryChart);
//    
//    SwapChart.OwlLineChartInit=OwlLineChartInit;
//    SwapChart.OwlLineChartInit(&SwapChart);
//    
//    IOWaitChart.OwlLineChartInit=OwlLineChartInit;
//    IOWaitChart.OwlLineChartInit(&IOWaitChart);
    
    
   // LoadChart.SetOwlLineChartArea(&LoadChart, LineCArea);
   // LoadChart.SetOwlLineChartWidthHeight(&LoadChart, LineCWidthHeight);

    
    initscr();
    cbreak();
    curs_set(0);
    
    
    OwlWin.SetScreenArea(&OwlWin, GetScreenXY());
    ChartWindow = newwin(GetScreenXY().y, GetScreenXY().x, 0, 0);
    DrawWindow(&OwlWin, con);
    
    overwrite(OwlWin.window, LoadChart.Chart);
//    overwrite(OwlWin.window, MemoryChart.Chart);
//    overwrite(OwlWin.window, SwapChart.Chart);
//    overwrite(OwlWin.window, IOWaitChart.Chart);
    
    PrePareLineChart(&LoadChart, LineCWidthHeight, LineCArea, LoadYAxis, LoadXAxis, "Load Avg");
//    PrePareLineChart(&MemoryChart, MemCWidthHeight, MemCArea, LoadYAxis, LoadXAxis, "Free RAM(Mb)");
//    PrePareLineChart(&SwapChart, SwapCWidthHeight, SwapCArea, LoadYAxis, LoadXAxis, "Swap Used(Mb)");
//    PrePareLineChart(&IOWaitChart, IOWaitCWidthHeight, IOWaitCArea, LoadYAxis, LoadXAxis, "IO Wait (%)");
    
    
    while (1) {
        char ch='p';
        SM->SetUpTime(SM);
        LineChartPlot(&LoadChart, ceil(SM->GetLoad1(SM)));
        
//        SM->SetFreeRam(SM);
//        LineChartPlot(&MemoryChart, ceil(SM->GetFreeRam(SM)));
//
//        SM->SetSwapUsed(SM);
//        LineChartPlot(&SwapChart, ceil(SM->GetSwapUsed(SM)));
//        
//        SM->SetIOWait(SM);
//        LineChartPlot(&IOWaitChart, ceil(SM->GetIOWait(SM)));
        if (KbHit()) {
            ch=ReadCh();
        }
        if(ch == 'p')
            sleep(1);
        else if(ch =='c')
            usleep(10000);
        else if(ch =='x')
            break;
        
    }
    nocbreak();
    echo();
    //S->EndSession(S);
    //PQfinish(con);
    endwin();
    return 0;
}
