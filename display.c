//
//  display.c
//  pgOWL
//
//  Created by Dinesh Kumar on 04/12/14.
//  Copyright (c) 2014 OpenSCG. All rights reserved.
//
#include "display.h"
#include "math.h"
#include "stdlib.h"
#include "string.h"
#include "unistd.h"
/*
 * Line Chart Specific Macros
 */

#define MAXAXISPOINTS   60

extern void FillingQueryHash();
/*
 *  Setter methods
 */
void SetOptionsArea(OwlWindow* w, Plot p) {
    
    w->OptionsArea = p;
}

void SetChartArea(OwlWindow* w, Plot p) {
    
    w->ChartArea = p;
}

void SetHeaderArea(OwlWindow* w, Plot p) {
    
    w->HeaderArea = p;
}

void SetSummaryArea(OwlWindow* w, Plot p) {

    w->SummaryArea = p;
}

void SetTimeArea(OwlWindow* w, Plot p) {
    
    w->TimeArea = p;
}

void SetScreenArea(OwlWindow* w, Plot p) {
    
    w->ScreenArea = p;
}

/*
 *  Getter methods
 */
Plot GetOptionsArea(OwlWindow* w) {
    
    return w->OptionsArea;
}

Plot GetChartArea(OwlWindow* w) {
    
    return w->ChartArea;
}

Plot GetTimeArea(OwlWindow* w) {
    
    return w->TimeArea;
}

Plot GetSummaryArea(OwlWindow* w) {
    
    return w->SummaryArea;
}

Plot GetHeaderArea(OwlWindow* w) {
    
    return w->HeaderArea;
}

Plot GetScreenArea(OwlWindow* w) {
    
    return w->ScreenArea;
}

void DrawChartWindow(OwlLineChart* C) {
    box(C->Chart, 0, 0);
    touchwin(C->Chart);
    wrefresh(C->Chart);
}

void OwlWindowInit(OwlWindow* w) {
    
    w->GetOptionsArea = GetOptionsArea;
    w->GetChartArea = GetChartArea;
    w->GetTimeArea = GetTimeArea;
    w->GetSummaryArea = GetSummaryArea;
    w->GetHeaderArea = GetHeaderArea;
    w->GetScreenArea = GetScreenArea;
    
    w->SetOptionsArea = SetOptionsArea;
    w->SetChartArea = SetChartArea;
    w->SetTimeArea = SetTimeArea;
    w->SetSummaryArea = SetSummaryArea;
    w->SetHeaderArea = SetHeaderArea;
    w->SetScreenArea = SetScreenArea;
    
    FillingQueryHash();
}


/*
 * Chart Methods
 */

Plot GetOwlLineChartArea(OwlLineChart* C){
    
    return C->ChartArea;
}

Plot GetOwlLineChartWidthHeight(OwlLineChart* C) {

    return C->ChartWidthHeight;
}

char* GetOwlLineChartName(OwlLineChart* C) {
    return C->Pref->ChartName;
}

void SetOwlLineChartArea(OwlLineChart* C, Plot P) {
    
    C->ChartArea = P;
}

void SetOwlLineChartWidthHeight(OwlLineChart* C, Plot P) {
    
    C->ChartWidthHeight = P;
}

void SetOwlLineChartName(OwlLineChart* C, char* Name) {
    strcpy(C->Pref->ChartName, Name);
}


void SetOwlLineChartSize(OwlLineChart* C, Plot Y, Plot X) {
    
    C->Pref->ChartHeight = Y;
    C->Pref->ChartWidth = X;
}

void SetOwlLineChartPosition(OwlLineChart* C, Plot Y, Plot X)
{
    C->Pref->ParWinYAxis = Y;
    C->Pref->ParWinXAxis = X;
}

void NewOwlLineChart(OwlLineChart* C, Plot ParWinYAxis, Plot ParWinXAxis) {

    C->Pref = (LineChartPref *) calloc(sizeof(LineChartPref), 1);
    
    
    C->Chart = newwin(ParWinYAxis.y, ParWinYAxis.x, ParWinXAxis.y, ParWinXAxis.x);
    
    /*
     * Chart Preferences Settings
     */
    
    
    C->Pref->Points = (LineDataPoints* ) calloc(sizeof(LineDataPoints), 1);
    C->Pref->CurrPlotPosition = (Plot *) calloc(sizeof(Plot), 1);
    
    /* Setting a default YUnitStep Counter to 2 */
    
    C->Pref->YUnitStepCounter = 2;
    C->Pref->CurrPlotPosition->x = 0;
    C->Pref->CurrPlotPosition->y = 0;
    C->SetOwlLineChartPosition(C, ParWinYAxis, ParWinXAxis);
}

void OwlLineChartInit(OwlLineChart* C) {
    
    C->GetOwlLineChartArea = GetOwlLineChartArea;
    C->GetOwlLineChartWidthHeight = GetOwlLineChartWidthHeight;
    C->GetOwlLineChartName = GetOwlLineChartName;
    
    C->SetOwlLineChartArea = SetOwlLineChartArea;
    C->SetOwlLineChartWidthHeight = SetOwlLineChartWidthHeight;
    C->SetOwlLineChartName = SetOwlLineChartName;
    C->SetOwlLineChartSize = SetOwlLineChartSize;
    C->SetOwlLineChartPosition = SetOwlLineChartPosition;
    
    C->DrawChartWindow = DrawChartWindow;
    C->NewOwlLineChart = NewOwlLineChart;
}


void ClearRoundTripPointValue(WINDOW* win, const Plot Point) {
    
    mvwprintw(win, 6, 10, "CURR (%d,%d)", Point.y, Point.x);
    if ((int)(Point.y%5)==0)
        mvwprintw(win, Point.y, Point.x, ".");
    else
        mvwprintw(win, Point.y, Point.x, " ");
}

void ChangePrevPointSymbol(WINDOW* win, const Plot Point) {
    
    /*
     * Skip the ChangePrevPointSymbol if it is the starting point
     * in the line chart
     */
    mvwprintw(win, 5, 10, "PREV (%d,%d)", Point.y, Point.x);
    if (Point.x!=0 && Point.y!=0)
    {
        mvwprintw(win, Point.y, Point.x, "*");
    }
}

//WINDOW* win, LineDataPoints *List, Plot Point,
unsigned int InsertDataPoint(OwlLineChart* Chart, unsigned int Value) {
    
    //static unsigned int NDataPoints;
    Plot PrevPoint;
    
    if (Chart->Pref->Points->Start==NULL) {
        Chart->Pref->Points->Start = (LineDataPoints *) calloc(sizeof(LineDataPoints), 1);
        Chart->Pref->Points->Start = Chart->Pref->Points;
        Chart->Pref->Points->Value = Value;
        Chart->Pref->Points->Point = *(Chart->Pref->CurrPlotPosition);
        Chart->Pref->Points->End = Chart->Pref->Points;
        Chart->Pref->Points->CurrentPoint = Chart->Pref->Points;
        Chart->Pref->Points->NextPoint = NULL;
        Chart->Pref->NoOfPlots=0;
        Chart->Pref->NoOfPlots++;
    }
    
    else if (Chart->Pref->NoOfPlots<(MAXAXISPOINTS+1)) {
        PrevPoint = Chart->Pref->Points->End->Point;
        Chart->Pref->Points->End->NextPoint = (LineDataPoints *) calloc(sizeof(LineDataPoints), 1);
        Chart->Pref->Points->End->NextPoint->Point = *(Chart->Pref->CurrPlotPosition);
        Chart->Pref->Points->End->NextPoint->Value = Value;
        Chart->Pref->Points->End->NextPoint->NextPoint = NULL;
        Chart->Pref->Points->End = Chart->Pref->Points->End->NextPoint;
        Chart->Pref->Points->CurrentPoint = Chart->Pref->Points->End;
        Chart->Pref->NoOfPlots++;
    }
    
    else if (Chart->Pref->NoOfPlots==(MAXAXISPOINTS+1)) {
        PrevPoint = *(Chart->Pref->CurrPlotPosition);
        
        if (Chart->Pref->Points->CurrentPoint->Point.x == Chart->Pref->Points->End->Point.x)
            Chart->Pref->Points->CurrentPoint = Chart->Pref->Points->Start;
        else
            Chart->Pref->Points->CurrentPoint = Chart->Pref->Points->CurrentPoint->NextPoint;
        
        /*
         * If the current position x reaches to it's end, and before
         * setting the x position to the starting position of the chart,
         * we need to empty it's previous position value
         *
         */
        ClearRoundTripPointValue(Chart->Chart, Chart->Pref->Points->CurrentPoint->Point);
        
        /*
         * Changing the CurrentPoint Position, and it's associated
         * value, to the latest
         *
         */
        
        Chart->Pref->Points->CurrentPoint->Value = Value;
        Chart->Pref->Points->CurrentPoint->Point = *(Chart->Pref->CurrPlotPosition);
        
        //Chart->Pref->NoOfPlots = MAXAXISPOINTS+1;
    }
    
    /*
     * By calling this procedure, we change the previous point plot symbol
     * as "*". This gives a bit progressive look to the line chart.
     */
    ChangePrevPointSymbol(Chart->Chart, PrevPoint);
    
    return Chart->Pref->NoOfPlots;
}

// , Plot Yaxis, Plot Xaxis, size_t Ystep, size_t Xstep

void DrawLineChart(OwlLineChart* Chart) {
    
    unsigned int y, x, YLabValWidth=4, XInitVal = 5;

    // Printing Chart Header
    //
    mvwprintw(Chart->Chart, Chart->Pref->ChartHeight.y-1, Chart->Pref->ChartWidth.x/2, "%s", Chart->Pref->ChartName);
    
    // Drawing YAxis
    //
    for (y=Chart->Pref->ChartHeight.y; y<=Chart->Pref->ChartWidth.y; y++) {
        
        mvwprintw(Chart->Chart, (int)y, Chart->Pref->ChartHeight.y, "%3d '",
                  ((Chart->Pref->YUnitStepCounter*Chart->Pref->ChartWidth.y)-(Chart->Pref->YUnitStepCounter*y)));
        
        /*
         *  Drawing horizon lines on YAxis, for every
         *  5 YAxis points.
         */
        if ((int)y%5 == 0) {
            for (x=Chart->Pref->ChartHeight.x+1; x<=Chart->Pref->ChartWidth.x+1; x++) {
                mvwprintw(Chart->Chart, (int)y, (int)(x+YLabValWidth), ".");
            }
        }
        /*
         * Otherwise, plot a white page character,
         * which will be helpful to clear all the values
         * while we Adjust the chart.
         */
        else {
            for (x=Chart->Pref->ChartHeight.x+1; x<=Chart->Pref->ChartWidth.x; x++) {
                mvwprintw(Chart->Chart, (int)y, (int)(x+YLabValWidth), " ");
            }
        }
        wrefresh(Chart->Chart);
    }
    
    /* For pretty print, taking one step forward
     * to start the xaxis line, and extending
     * the line by one step.
     */
    for (x=Chart->Pref->ChartHeight.x+1; x<=Chart->Pref->ChartWidth.x+1; x++) {
        
        /*
         * Drawing X-Axis one line below ot the origin point,
         * which do not colloid with the X-Axis plot values
         */
        
        mvwprintw(Chart->Chart, Chart->Pref->ChartWidth.y+1, (int)(x+YLabValWidth), "-");
        /*
         *  Drawing a divider on XAxis, for every
         *  5 XAxis points.
         */
        if ((int)(x-1)%5 == 0 && x-1 != Chart->Pref->ChartHeight.x) {
            mvwprintw(Chart->Chart, Chart->Pref->ChartWidth.y+1, (int)(x+YLabValWidth), "!");
            mvwprintw(Chart->Chart, Chart->Pref->ChartWidth.y+2, (int)(x+YLabValWidth), "%d", XInitVal);
            XInitVal = XInitVal + 5;
        }
    }
}

// WINDOW *win, LineDataPoints* List, Plot XAxis, unsigned int Ystep)
//
void AdjustLineChart(OwlLineChart* Chart) {
    
    LineDataPoints *tmp = Chart->Pref->Points->Start;
    
    if (tmp==NULL) {
        return;
    }
    
    while (tmp) {
        tmp->Point.y = (Chart->Pref->ChartWidth.y) - tmp->Value/Chart->Pref->YUnitStepCounter;
        mvwprintw(Chart->Chart, tmp->Point.y, tmp->Point.x, "*");
        tmp = tmp->NextPoint;
    }
}

//, LineDataPoints* List, Plot Yaxis, Plot Xaxis, Plot* CurrentPos, unsigned int *Ystep,

 void LineChartPlot(OwlLineChart* Chart, int Value) {
    
    int YLabValWidth = 4;
    
    if (!Chart->Pref->CurrPlotPosition->x)
        Chart->Pref->CurrPlotPosition->x = Chart->Pref->ChartHeight.x + YLabValWidth + 1;
    
    /*
     * Here the code deal with, to rearrange the line chart
     * If the plot value is beyond the yaxis maximum value.
     *
     * As an implementation, we are going to calculate the difference
     * between the yaxix max value, and this new plot value, and going
     * to the add this difference to Yaxis lable values, and rearranging
     * the total collected LineDataPoints, and displaying new graph,
     * before ploting this new higher value.
     *
     */
    
    if (Value > ((Chart->Pref->ChartWidth.y*(Chart->Pref->YUnitStepCounter))-(Chart->Pref->ChartHeight.y*(Chart->Pref->YUnitStepCounter)))) {
  
        /*
         * Rearranging the Ystep, by finding, how
         * we can arrange or plot this higher value
         * in our chart area. That is, here the no.of
         * horizontal chart Plots based on Xaxis.y-Yaxis.y
         */
        
        Chart->Pref->YUnitStepCounter = (unsigned int) ceil((float)Value/(Chart->Pref->ChartWidth.y-Chart->Pref->ChartHeight.x));
        DrawLineChart(Chart);
        AdjustLineChart(Chart);
    }

     
    Chart->Pref->CurrPlotPosition->y = (Chart->Pref->ChartWidth.y) - (Value/(Chart->Pref->YUnitStepCounter));
    
    /*
     * If the current position x, reaches to it's end, then
     * setting the x position to the startring position of the
     * chart
     *
     */
    
    if (Chart->Pref->CurrPlotPosition->x == (Chart->Pref->ChartWidth.x-Chart->Pref->ChartHeight.x +
        Chart->Pref->ChartHeight.x + YLabValWidth + 2)) {
        Chart->Pref->CurrPlotPosition->x = Chart->Pref->ChartHeight.x + YLabValWidth + 1;
    }
    
    InsertDataPoint(Chart, Value);
    /*
     * Printing the progress indicator line
     */
    
    mvwprintw(Chart->Chart, Chart->Pref->CurrPlotPosition->y, Chart->Pref->CurrPlotPosition->x, "O");
    Chart->Pref->CurrPlotPosition->x++;
    
    wrefresh(Chart->Chart);
}

void PrePareLineChart(OwlLineChart* Chart, Plot LinesCols, Plot Area, Plot Height, Plot Width, char* ChartName) {
    
    Chart->OwlLineChartInit=OwlLineChartInit;
    Chart->OwlLineChartInit(Chart);
    
    Chart->NewOwlLineChart(Chart, LinesCols, Area);
    Chart->SetOwlLineChartName(Chart, ChartName);
    Chart->SetOwlLineChartSize(Chart, Height, Width);
    
    Chart->DrawChartWindow(Chart);
    DrawLineChart(Chart);
    
}