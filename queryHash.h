//
//  queryHash.h
//  pgOWL
//
//  Created by Dinesh Kumar on 05/12/14.
//  Copyright (c) 2014 OpenSCG. All rights reserved.
//

#ifndef pgOWL_queryHash_h
#define pgOWL_queryHash_h

#define QUERYHASHSIZE 20
#include <stdlib.h>

typedef struct {
    void **Key;
    void **Query;
} QueryHash;

QueryHash* NewQueryHash() {
    
    QueryHash* hash = calloc(1, sizeof(QueryHash));
    hash->Key = calloc(QUERYHASHSIZE, sizeof(void *));
    hash->Query = calloc(QUERYHASHSIZE, sizeof(void *));
    return hash;
}

int QueryHashIndex (QueryHash* hash, void* key) {
    
    int index = (int)key%QUERYHASHSIZE;
    
    while (hash->Key[index] && hash->Key[index] != key)
        index = (index + 1) % QUERYHASHSIZE;

    return index;
}

void QueryHashInsert (QueryHash* hash, void* key, void* query) {
    
    int index = QueryHashIndex(hash, key);
    hash->Key[index] = key;
    hash->Query[index] = query;
}

void* QueryHashLookup (QueryHash* hash, void* key) {
    
    int index = QueryHashIndex(hash, key);
    return hash->Query[index];
}

#endif
