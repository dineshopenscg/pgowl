//
//  serverconnection.c
//  pgOWL
//
//  Created by Dinesh Kumar on 04/02/15.
//  Copyright (c) 2015 OpenSCG. All rights reserved.
//

#include "serverconnection.h"


/*
 * Get Methods
 */

const char* GetUserName(ServerConnection* S) {
    return S->UserName;
}

const char* GetPassword(ServerConnection* S) {
    return S->Password;
}

const char* GetHostIP(ServerConnection* S) {
    struct in_addr IP;
    IP.s_addr = (unsigned int)S->HostIp;
    return inet_ntoa(IP);
}

const unsigned long GetHostIPNL(ServerConnection* S) {
    return S->HostIp;
}

sockaddr_in GetSockIn(ServerConnection* S) {
    return S->SockIn;
}

LIBSSH2_SESSION* GetSession(ServerConnection* S) {
    return S->Session;
}

LIBSSH2_CHANNEL* GetChannel(ServerConnection* S) {
    return S->Channel;
}

/*
 * Set Methods
 */

void SetUserName(ServerConnection* S, const char* U) {
    S->UserName = U;
}

void SetPassword(ServerConnection* S, const char* P) {
    S->Password = P;
}

void SetHostIP(ServerConnection* S, const char* IP) {
    S->HostIp = inet_addr(IP);
}

/*
 * Functional Methods
 */

int InitSsh(ServerConnection* S) {
    
    int rc,con;
    
    rc = libssh2_init(0);
    if (rc) {
        fprintf(stderr, "%d-Libssh2 initialization failed",rc);
        return 0;
    }
    
    // Setting socket options
    //
    S->Sock = socket(AF_INET, SOCK_STREAM, 0);
    S->SockIn.sin_family = AF_INET;
    S->SockIn.sin_addr.s_addr = (in_addr_t) S->GetHostIPNL(S);
    S->SockIn.sin_port = htons(22);
    
    con = connect(S->Sock,(struct sockaddr*)(&(S->SockIn)), sizeof(struct sockaddr_in));
    
    if (con != 0) {
        long err;
        fprintf(stderr, "Failed to create local socket %d",errno);
        err=errno;
        if (err==ECONNREFUSED) {
            printf("Testing");
        }
        return 0;
    }
    
    return 1;
}

int WaitSocket(ServerConnection* S) {
    struct timeval timeout;
    int rc;
    fd_set fd;
    fd_set *writefd=NULL;
    fd_set *readfd=NULL;
    int dir;
    
    timeout.tv_sec = 10;
    timeout.tv_usec = 0;
    
    FD_ZERO(&fd);
    FD_SET(S->Sock, &fd);
    
    /* Now make sure we wait in the correct direction */
    dir = libssh2_session_block_directions(S->Session);
    
    if (dir & LIBSSH2_SESSION_BLOCK_INBOUND)
        readfd = &fd;
    
    if (dir & LIBSSH2_SESSION_BLOCK_OUTBOUND)
        writefd = &fd;
    
    rc = select(S->Sock + 1, readfd, writefd, NULL, &timeout);
    
    return rc;
}

int StartSession(ServerConnection* S) {
    
    S->Session = libssh2_session_init();

    //libssh2_session_set_blocking(S->Session, 0);
    if (libssh2_session_handshake(S->Session, S->Sock)) {
        fprintf(stderr, "Error enabling SSH connection");
        return 0;
    }
    
    if (libssh2_userauth_password_ex(S->Session, S->UserName,
                                     (unsigned int) strlen(S->UserName),S->Password,
                                     (unsigned int) strlen(S->Password),NULL)==0) {
       // printf("Credentials Are Fine");
        ;
    }
    
    return 1;
}

int StartChannel(ServerConnection* S) {
    
    S->Channel = libssh2_channel_open_session(S->Session);
    
    if (!S->Channel) {
        fprintf(stderr, "%d-Failed to open a shell session", errno);
        return 0;
    }
    
    return 1;
}

char* ExecuteCmd(ServerConnection* S,const char* Cmd) {
    
    char *Result=(char *)calloc(sizeof(char), 100);
    int rc;
    S->StartChannel(S);
    
    while ((rc = libssh2_channel_exec(S->Channel, Cmd))==LIBSSH2_ERROR_EAGAIN) {
        WaitSocket(S);
    }
    
    if (rc !=0) {
        fprintf(stderr, "%d-Not able to execute command",errno);
        return NULL;
    }
    
    
    for ( ;  ; ) {
        int rc;
        
        do {
            
            rc = libssh2_channel_read(S->Channel,Result,(ssize_t)100);
            
            /*if (rc > 0) {
                fprintf(stderr, "We Read");
            }
            
            else {
                if (rc != LIBSSH2_ERROR_EAGAIN) {
                    fprintf(stderr, "Libssh2_channel_read return %d",rc);
                }
            }*/
            
        } while (rc>0);
        
        if (rc == LIBSSH2_ERROR_EAGAIN) {
            WaitSocket(S);
        }
        else
            break;
    }
    
    while ((rc = libssh2_channel_close(S->Channel)) == LIBSSH2_ERROR_EAGAIN) {
        WaitSocket(S);
    }
    
    libssh2_channel_free(S->Channel);
    /*
     * XXX: Make sure to clear the Result Memory
     */
    return Result;
}


int EndSession(ServerConnection* S) {

    libssh2_session_disconnect(S->Session, "Closing SSH Connection");
    libssh2_session_free(S->Session);
    close(S->Sock);
    libssh2_exit();

    /*
     * XXX: Need to free the Username, Password
     * char fields.
     */
    return 1;

}

void ServerConnInit(ServerConnection* S) {

    S->UserName = (const char *) calloc(sizeof(char), MAXUSERNAME);
    S->Password = (const char *) calloc(sizeof(char), MAXPASSLENGTH);
    
    // Get
    S->GetChannel = GetChannel;
    S->GetHostIP = GetHostIP;
    S->GetHostIPNL = GetHostIPNL;
    S->GetPassword = GetPassword;
    S->GetSession = GetSession;
    S->GetSockIn = GetSockIn;
    S->GetUserName = GetUserName;
    
    // Set
    S->SetHostIP = SetHostIP;
    S->SetPassword = SetPassword;
    S->SetUserName = SetUserName;
    
    // Functional
    S->InitSsh = InitSsh;
    S->StartSession = StartSession;
    S->EndSession = EndSession;
    S->StartChannel = StartChannel;
    S->ExecuteCmd = ExecuteCmd;

}