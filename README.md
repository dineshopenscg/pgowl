# pgOWL #

pgOWL is the tool based on libssh2, ncurses. This gives the DBAs an instance online access to the production servers, and display the graphs in their console. Isin't cool :)

### How it works. ###

* Install the pgOWL in your localmachine, and build it using respective flags (-lssh2, -lncurses).
* Run the pgOWL, by providing your remote "SSH" credentials.
* Then, you are done.
* All the linecharts, you see in your console will automatically adjusts to their new chart levels.

It's still in development phase, as this is a just proof of concept.