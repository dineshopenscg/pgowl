//
//  servermetrics.h
//  pgOWL
//
//  Created by Dinesh Kumar on 06/02/15.
//  Copyright (c) 2015 OpenSCG. All rights reserved.
//

#ifndef pgOWL_servermetric_h
#define pgOWL_servermetric_h

#define MAXHOSTNAMELEN 256
#define MAXUPTIMELEN 256

#include "serverconnection.h"

typedef struct ServerMetrics ServerMetrics;

struct ServerMetrics {
    
    ServerConnection* ServerConn;
    char* HostName;
    char* UpTime;
    float Load1, Load5, Load15;
    float FreeRam, SwapUsed;
    float IOWait;
    char** MemInfo;
    char** VmInfo;
    char** IoStat;
    
    
    void (*ServerMetricInit)(ServerMetrics*);
    /*
     * Get Methods
     */
    char* (*GetHostName)(ServerMetrics*);
    char* (*GetUpTime)(ServerMetrics*);
    float (*GetLoad1)(ServerMetrics*);
    float (*GetLoad5)(ServerMetrics*);
    float (*GetLoad15)(ServerMetrics*);
    float (*GetFreeRam)(ServerMetrics*);
    float (*GetSwapUsed)(ServerMetrics*);
    float (*GetIOWait)(ServerMetrics*);
    char** (*GetDiskUsage)(ServerMetrics*);
    char** (*GetMemInfo)(ServerMetrics*);
    char** (*GetVmInfo)(ServerMetrics*);
    char** (*GetIoStat)(ServerMetrics*);
    
    /*
     * Set Methods
     */
    
    void (*SetHostName)(ServerMetrics*);
    void (*SetUpTime)(ServerMetrics*);
    void (*SetDiskUsage)(ServerMetrics*);
    void (*SetMemInfo)(ServerMetrics*);
    void (*SetVmInfo)(ServerMetrics*);
    void (*SetIoStat)(ServerMetrics*);
    void (*SetServerConn)(ServerMetrics*, ServerConnection*);
    void (*SetFreeRam)(ServerMetrics*);
    void (*SetSwapUsed)(ServerMetrics*);
    void (*SetIOWait)(ServerMetrics*);
};

#endif
