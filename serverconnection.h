//
//  serverconnection.h
//  pgOWL
//
//  Created by Dinesh Kumar on 04/02/15.
//  Copyright (c) 2015 OpenSCG. All rights reserved.
//

#ifndef pgOWL_serverconnection_h
#define pgOWL_serverconnection_h

#include <libssh2.h>

#ifdef HAVE_WINDOWS_H
#include <windows.h>
#endif

#ifdef HAVE_WINSOCK2_H
#include <winsock2.h>
#endif

#ifdef HAVE_SYS_SOCKET_H
#include <sys/socket.h>
#endif

#include <unistd.h>
#include <arpa/inet.h>

#define MAXUSERNAME 128
#define MAXPASSLENGTH 128
#define MAXHOSTIPADDR   20

#include <sys/types.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <ctype.h>
#include <netinet/in.h>
#include <stdlib.h>

typedef struct ServerConnection ServerConnection;
typedef struct sockaddr_in sockaddr_in;

struct ServerConnection{
    const char *UserName;
    const char *Password;
    unsigned long HostIp;
    sockaddr_in SockIn;
    int Sock;
    LIBSSH2_SESSION *Session;
    LIBSSH2_CHANNEL *Channel;
    
    
    // Init method
    void (*ServerConnInit)(ServerConnection*);
    
    // Get methods
    //
    const char* (*GetUserName)(ServerConnection*);
    const char* (*GetPassword)(ServerConnection*);
    const char* (*GetHostIP)(ServerConnection*);
    const unsigned long (*GetHostIPNL)(ServerConnection*);
    sockaddr_in (*GetSockIn)(ServerConnection*);
    LIBSSH2_SESSION* (*GetSession)(ServerConnection*);
    LIBSSH2_CHANNEL* (*GetChannel)(ServerConnection*);
    
    // Set methods
    //
    void (*SetUserName)(ServerConnection*, const char*);
    void (*SetPassword)(ServerConnection*, const char*);
    void (*SetHostIP)(ServerConnection*, const char*);

    
    // Functional methods
    int (*InitSsh)(ServerConnection*);
    int (*CheckSock)(ServerConnection*);
    int (*StartSession)(ServerConnection*);
    int (*StartChannel)(ServerConnection*);
    char* (*ExecuteCmd)(ServerConnection*, const char*);
    int (*EndSession)(ServerConnection*);
    int (*WaitSocket)(ServerConnection*);
};

#endif
