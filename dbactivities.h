//
//  dbactivities.h
//  pgOWL
//
//  Created by Dinesh Kumar on 05/12/14.
//  Copyright (c) 2014 OpenSCG. All rights reserved.
//

#ifndef pgOWL_dbactivities_h
#define pgOWL_dbactivities_h

#include "queryHash.h"
#include "libpq-fe.h"

QueryHash* queryHash;

void FillingQueryHash() {
    
    queryHash = NewQueryHash();
    QueryHashInsert (queryHash, "DBTIMESTAMP", "SELECT now();");

    QueryHashInsert (queryHash, "TOP10BIGTABLES", "SELECT relname, pg_size_pretty(relsize) "		\
                     "FROM "																		\
                     "( "                                                                           \
                     "SELECT relname, "                                                             \
                     "(pg_total_relation_size(nspname||'.'||relname::regclass)) relsize "           \
                     "FROM "																		\
                     "pg_class c, pg_namespace n WHERE n.oid=c.relnamespace "                       \
                     "AND relkind ='r' AND nspname !~ "                                             \
                     "'^pg_toast|^information_schema|^pg_catalog' ORDER BY 2 DESC LIMIT 10) foo;");
    
    QueryHashInsert (queryHash, "IDLECONNECTIONS", "SELECT client_addr, "               \
        "COALESCE(AVG(now()-query_start), '00:00:00'::INTERVAL) avg_query_idle, "       \
        "COALESCE(MAX(now()-query_start), '00:00:00'::INTERVAL) max_query_idle, "		\
        "COALESCE(MIN(now()-query_start), '00:00:00'::INTERVAL) min_query_idle, "		\
        "COUNT(*) count "																\
        "FROM pg_stat_activity WHERE state ='idle' GROUP BY 1;");
    
    QueryHashInsert (queryHash, "ACTIVECONNECTIONS", "SELECT client_addr, "                             \
                     "COALESCE(AVG(now()-query_start), '00:00:00'::INTERVAL) avg_query_active, "        \
                     "COALESCE(MAX(now()-query_start), '00:00:00'::INTERVAL) max_query_active, "		\
                     "COALESCE(MIN(now()-query_start), '00:00:00'::INTERVAL) min_query_active, "		\
                     "COUNT(*) count "                                                                  \
                     "FROM pg_stat_activity WHERE state ='active' GROUP BY 1;");
    /*
    QueryHashInsert (queryHash, "LONGRUNNINGTRX", "");
    QueryHashInsert (queryHash, "LONGRUNNINGQRY", "");
    QueryHashInsert (queryHash, "AUTOVACUUM", "");
    QueryHashInsert (queryHash, "STATACTIVITY", "");
    QueryHashInsert (queryHash, "WAITINGTRX", "");
    QueryHashInsert (queryHash, "CONNECTIONSUMMARY", "");
    QueryHashInsert (queryHash, "SEARCHQUERIESLIKETHIS", "");
     */
}

char* GetDbScalar(PGconn* con, const char* query) {
    
    static char* scalar;
    PGresult *result = PQexec(con, query);
    scalar = PQgetvalue(result, 0, 0);
    PQclear(result);
    result = NULL;
    return scalar;
}


#endif